namespace Owls

open System
open MonoTouch.UIKit
open MonoTouch.Foundation
open PissedOffOwls

[<Register ("AppDelegate")>]
type AppDelegate () =
    inherit UIApplicationDelegate ()

    // This method is invoked when the application is ready to run.
    override this.FinishedLaunching (app) =
        let scale = UIScreen.MainScreen.Scale
        let height = UIScreen.MainScreen.Bounds.Height * scale
        let width = UIScreen.MainScreen.Bounds.Width * scale
        let game = new PissedOffOwl(height, width)
        game.Run()

module Main =
    [<EntryPoint>]
    let main args =
        UIApplication.Main (args, null, "AppDelegate")
        0

